using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyZombie : EnemyCharacter
{

    public override void GetDefaultDamage(Color color)
    {
        base.GetDefaultDamage(color);

        Debug.Log("zombie got hit");

        enabled = true;

        if(transform.parent != null)
        {
            var group = transform.parent.GetComponent<ZombiesGroup>();

            if(group != null)
            {
                group.ZombieHit();
            }
        }
    }

    public override void Death()
    {
        base.Death();

        animator.enabled = false;

        agent.enabled = false;
    }
}
