using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paintball : Colorable
{
    [SerializeField] private LayerMask paintMask;

    [SerializeField] private new Renderer renderer;

    [SerializeField] private float splashRadius;

    [SerializeField] private PaintSplashEffect splashEffect_prefab;

    [SerializeField] private PaintType paintType = PaintType.LEAKING_SPOT;

    [SerializeField] private AnimationCurve speedCurve;

    [SerializeField] private float maxFlyTime = 3;

    private float flyTime;

    private float speed;


    public void Shoot(Vector3 dir)
    {
        transform.forward = dir;
    }

    private void FixedUpdate()
    {
        speed = speedCurve.Evaluate(flyTime);

        transform.position += transform.forward * speed * Time.fixedDeltaTime;

        flyTime += Time.fixedDeltaTime;

        if (flyTime > maxFlyTime) Destroy(gameObject);
    }

    public override void SetColor(Color color)
    {
        base.SetColor(color);

        var mat = renderer.material;

        mat.SetColor(colorProp, color);
        mat.SetColor(emissionColorProp, color * .2f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(HitProcess(collision));
    }

    private IEnumerator HitProcess(Collision collision)
    {
        if (!renderer.enabled) yield break;

        var paintable = collision.collider.GetComponent<PaintableObject>();

        if(paintable)
        {
            renderer.enabled = false;

            var effect = Instantiate(splashEffect_prefab);

            effect.SetColor(color);

            effect.transform.position = transform.position;

            var origin = Camera.main.transform.position;

            var ray = new Ray(origin, (transform.position - origin).normalized);


            if(Physics.Raycast(ray, /*splashRadius,*/ out RaycastHit hit, Mathf.Infinity, paintMask))
            {
                //Debug.DrawRay(ray.origin, ray.direction * hit.distance, color, 20);
                //Debug.DrawRay(hit.point, hit.normal, color, 20);

                FindObjectOfType<Control>().TryPaint(hit, color, paintType);
            }

            yield return new WaitForSeconds(.25f);

            gameObject.SetActive(false);
        }

    }
}
