using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colorable : MonoBehaviour
{
    public static string colorProp = "_Color";
    public static string emissionColorProp = "_EmissionColor";

    public Color color;

    public virtual void SetColor(Color color)
    {
        this.color = color;
    }
}
