using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintableObject : MonoBehaviour
{
    private const string PaintTex = "_PaintTex";

    public bool inited;

    [HideInInspector] public PaintCanvas canvas;

    public float paintScale = 1.0f;

    public Action<Color> OnGetPaint;

    public void Init(PaintCanvas paintCanvas)
    {
        inited = true;

        canvas = paintCanvas;

        paintCanvas.paintableObject = this;

        GetComponent<Renderer>().material.SetTexture(PaintTex, paintCanvas.paintTexture);
    }
}
