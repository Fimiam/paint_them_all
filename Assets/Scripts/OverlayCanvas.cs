using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OverlayCanvas : MonoBehaviour
{
    public static OverlayCanvas Instance;

    [SerializeField] private Image loadOverlay, hitOverlay;

    private void Awake()
    {
        Instance = this;

        var col = loadOverlay.color;

        col.a = 1.0f;

        loadOverlay.color = col;

        loadOverlay.DOFade(0, .7f);
    }

    public void ShowHit()
    {
        hitOverlay.DOKill();

        hitOverlay.DOFade(.5f, .3f).onComplete += () => hitOverlay.DOFade(0, .5f);
    }

    public void InitReloadScene(bool dead = false)
    {
        if(dead)
        {
            var c = loadOverlay.color;

            c.g = c.b = 0;

            loadOverlay.color = c;
        }

        loadOverlay.DOFade(1, .4f).onComplete += () => SceneManager.LoadScene(0);
    }
}
