using RagdollMecanimMixer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyCharacter : DestroyableObject
{
    [SerializeField] protected RamecanMixer ramecan;

    [SerializeField] protected NavMeshAgent agent;

    [SerializeField] protected Animator animator;

    [SerializeField] private float damageDistance = 1.0f;

    [SerializeField] private float damageRate = 2;

    [SerializeField] private float animSpeed = .26f, agentRelatedSpeed = .4f;

    float toNextDamage;

    public bool activableOnStart = true;

    protected virtual void Start()
    {
        var targetPos = Player.Instance.transform.position;

        agent.SetDestination(targetPos);

        toNextDamage = damageRate;

    }


    protected virtual void Update()
    {
        toNextDamage -= Time.deltaTime;

        if(toNextDamage < 0)
        {
            if(Vector3.Distance(Player.Instance.transform.position, transform.position) < damageDistance)
            {
                Player.Instance.GetDamage();
            }

            toNextDamage = damageRate;
        }

        animator.speed = agent.speed / agentRelatedSpeed * animSpeed;

        var movement = agent.velocity.magnitude / agent.speed;

        animator.SetFloat("movementSpeed", movement);
    }

    public override void Death()
    {
        ramecan.BeginStateTransition("dead");

        enabled = false;

        EnemiesController.Instance.EnemyDied(this);
    }
}
