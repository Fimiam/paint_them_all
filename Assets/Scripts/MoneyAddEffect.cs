using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoneyAddEffect : MonoBehaviour
{
    [SerializeField] private RectTransform rectTransform;

    public int moneyToAdd;

    public void AddMoney(Vector2 rectPos, Vector3 targetPos, int value)
    {
        moneyToAdd = value;

        rectTransform.anchoredPosition = rectPos;

        var pos = rectTransform.localPosition;

        pos.z = 0;

        rectTransform.localPosition = pos;

        transform.DOMove(targetPos, .8f).SetEase(Ease.Linear).onComplete += () =>
        {
            MoneyPanel.Instance.AddRealMoney(value);

            gameObject.SetActive(false);
        };
    }
}
