using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public static Control Instance;

    [SerializeField] private PaintCanvas paintCanvas_prefab;

    public Color debugColor;

    [SerializeField] private LayerMask paintHitMask;

    [SerializeField] private Paintball paintball_prefab;

    [SerializeField] private bool debugPaint;

    int canvases;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Application.targetFrameRate = 60;

        var level = PlayerPrefs.GetInt("level", 1);

        if(level == 3)
        {
            FindObjectOfType<RateUsPopup>().Show();
        }
    }

    public void TryPaint(PaintableObject paintable, Vector2 textureCoord, Color color, PaintType paint = PaintType.LEAKING_SPOT, bool damagable = true)
    {
        if (paintable)
        {
            if (!paintable.inited)
            {

                var canvas = Instantiate(paintCanvas_prefab);

                canvas.transform.position = Vector3.up * 25 + Vector3.right * (canvas.camera.orthographicSize * 2.0f + 5.0f) * canvases;

                canvas.Setup();

                paintable.Init(canvas);

                canvases++;
            }

            paintable.canvas.Paint(textureCoord, color, paint, damagable);
        }
    }

    public void TryPaint(RaycastHit hit, Color color, PaintType paint = PaintType.LEAKING_SPOT, bool damagable = true)
    {
        var paintable = hit.collider.GetComponent<PaintableObject>();

        //Debug.Log(hit.textureCoord);

        //Debug.Log("234");

        if (paintable)
        {
            if (!paintable.inited)
            {

                var canvas = Instantiate(paintCanvas_prefab);

                canvas.transform.position = Vector3.up * 25 + Vector3.right * (canvas.camera.orthographicSize * 2.0f + 5.0f) * canvases;

                canvas.Setup();

                paintable.Init(canvas);

                canvases++;
            }

            paintable.canvas.Paint(hit.textureCoord, color, paint, damagable);
        }
    }

    void Update()
    {
        if (debugPaint)
        {
            if (Input.GetMouseButton(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, paintHitMask))
                {
                    var paintable = hit.collider.GetComponent<PaintableObject>();

                    if (paintable)
                    {
                        if (!paintable.inited)
                        {
                            var canvas = Instantiate(paintCanvas_prefab);

                            canvas.transform.position = Vector3.up * 25 + Vector3.right * (canvas.camera.orthographicSize * 2.0f + 5.0f) * canvases;

                            canvas.Setup();

                            paintable.Init(canvas);

                            canvases++;
                        }

                        paintable.canvas.Paint(hit.textureCoord, debugColor, PaintType.SPOT);
                    }
                }
            }
        }
        //else
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //        var ball = Instantiate(paintball_prefab);

        //        ball.transform.position = Camera.main.transform.position;

        //        ball.Shoot(ray.direction);

        //        //var rb = ball.GetComponent<Rigidbody>();

        //        //rb.AddForce(ray.direction * 145, ForceMode.Impulse);

        //        //if (Physics.Raycast(ray, out RaycastHit hit))
        //        //{
        //        //}
        //    }
        //}


    }
}
