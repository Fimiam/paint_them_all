using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryItem : MonoBehaviour
{
    public float trackPercent;

    public void Setup()
    {
        var track = FindObjectOfType<GalleryTrack>();

        trackPercent = track.GetPercentOnTrack(transform);
    }
}
