using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gallery : MonoBehaviour
{
    [SerializeField] private GalleryCamera galleryCamera;

    public List<GalleryItem> items;

    public int activeItemIndex;

    private void Start()
    {
        items.ForEach(i => i.Setup());

        galleryCamera.Setup();
    }

    public int GetClosestItem(float percent)
    {
        int result = 0;


        float min = float.MaxValue;

        for (int i = 0; i < items.Count; i++)
        {
            var delta = Mathf.DeltaAngle(percent * 360, items[i].trackPercent * 360);

            if(delta < min)
            {
                min = delta;
                result = i;
            }
        }


        return result;
    }
}
