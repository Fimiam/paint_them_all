using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryCamera : MonoBehaviour
{
    [Range(0, 1)]
    public float currentPercent;
    [SerializeField] private GalleryTrack track;
    [SerializeField] private float smooth;

    [SerializeField] private float sens;

    private float targetPercent;


    private float currentDistance;

    private Gallery gallery;

    public void Setup()
    {
        gallery = FindObjectOfType<Gallery>();

        targetPercent = currentPercent = gallery.items[gallery.activeItemIndex].trackPercent;

        currentDistance = currentPercent * track.length;

        var sample = track.GetTrackPoint(currentPercent);

        transform.position = sample.position;
        transform.rotation = Quaternion.LookRotation(sample.up);
    }



    private void Update()
    {
        var input = sens * InputController.PlayerInput.x;

        currentDistance += input;

        if(currentDistance > track.length)
        {
            currentDistance = currentDistance - track.length;
        }

        if(currentDistance < 0)
        {
            currentDistance = track.length + currentDistance;
        }

        if (InputController.Hold)
        {
            currentPercent = targetPercent = currentDistance / track.length;
            gallery.activeItemIndex = gallery.GetClosestItem(currentPercent); 
        }
        else
        {
            var p = Mathf.MoveTowardsAngle(currentPercent, gallery.items[gallery.activeItemIndex].trackPercent, smooth);
            currentPercent = p / 360.0f;
        }
    }

    private void LateUpdate()
    {
        var sample = track.GetTrackPoint(currentPercent);

        transform.position = sample.position;
        transform.rotation = Quaternion.LookRotation(sample.up);
    }
}
