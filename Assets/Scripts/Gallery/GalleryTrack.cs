using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryTrack : MonoBehaviour
{
    [SerializeField] SplineComputer spline;
    [SerializeField] SplineProjector splineProjector;

    public float length;

    private void Awake()
    {
        length = spline.CalculateLength();
    }

    public float GetPercentOnTrack(Transform transform)
    {
        var percent = 0.0f;

        splineProjector.projectTarget = transform;
        splineProjector.CalculateProjection();

        percent = (float)splineProjector.result.percent;

        return percent;
    }

    public SplineSample GetTrackPoint(float percent)
    {
        return spline.Evaluate(percent);
    }
}
