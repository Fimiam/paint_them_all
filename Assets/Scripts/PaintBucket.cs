using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintBucket : MonoBehaviour
{
    private void OnMouseDown()
    {
        FindObjectOfType<BrushDeformable>().IntoTheBucket(this, transform.GetComponentInChildren<Colorable>().color);
    }
}
