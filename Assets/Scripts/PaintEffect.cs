using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PaintEffect : MonoBehaviour
{
    public abstract void SetColor(Color color);

    public abstract void Play();
}
