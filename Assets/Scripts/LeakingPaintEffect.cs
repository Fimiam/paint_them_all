using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeakingPaintEffect : PaintEffect
{
    [SerializeField] private new ParticleSystem particleSystem;

    public override void Play()
    {
        
    }

    public override void SetColor(Color color)
    {
        var psr = particleSystem.GetComponent<ParticleSystemRenderer>();

        psr.material.color = color;
    }
}
