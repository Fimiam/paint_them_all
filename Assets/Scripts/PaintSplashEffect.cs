using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSplashEffect : Colorable
{
    public PaintType paintType;

    [SerializeField] private new ParticleSystem particleSystem;

    [SerializeField] private LayerMask paintMask;

    [SerializeField] private AudioSource splashSource;

    private List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

    private void Start()
    {
        SetColor(color);
    }

    public override void SetColor(Color color)
    {
        base.SetColor(color);

        var partsMat = particleSystem.GetComponent<ParticleSystemRenderer>().material;

        partsMat.SetColor(colorProp, color);
        partsMat.SetColor(emissionColorProp, color * .6f);
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = particleSystem.GetCollisionEvents(other, collisionEvents);

        for (int i = 0; i < numCollisionEvents; i++)
        {
            var norm = collisionEvents[i].normal;
            var pos = collisionEvents[i].intersection;

            var ray = new Ray(pos - norm * .03f, -norm);

            if(Physics.Raycast(ray, out RaycastHit hit, 99, paintMask))
            {
                Control.Instance.TryPaint(hit, color, PaintType.PARTICLE_DROP, false);
            }
        }
    }
}
