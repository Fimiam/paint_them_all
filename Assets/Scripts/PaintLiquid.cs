using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintLiquid : Colorable
{
    [SerializeField] private MeshRenderer meshRenderer;

    private void Start()
    {
        var mat = meshRenderer.material;

        mat.SetColor(colorProp, color);
        mat.SetColor(emissionColorProp, color * .2f);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject);
    }
}
