using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class MagnGlass : MonoBehaviour
{
    [SerializeField] private SplineComputer path;

    [SerializeField] private float speed = 1;

    private float distance, lenght;

    private SplineSample splineSample;

    private float toNextMoney;


    void Start()
    {
        splineSample = path.Evaluate(distance);

        var pos = splineSample.position;

        transform.position = pos;

        lenght = path.CalculateLength();

        StartCoroutine(Process());
    }

    private IEnumerator Process()
    {
        Player.Instance.enabled = false;

        toNextMoney = Random.Range(.2f, .6f);

        while (distance < lenght)
        {
            splineSample = path.Evaluate(distance / lenght);

            toNextMoney -= Time.deltaTime;

            if(toNextMoney < 0)
            {
                MoneyPanel.Instance.AddMoney(transform.position , 100);

                toNextMoney = Random.Range(.2f, .6f);
            }

            yield return null;


            distance += speed * Time.deltaTime;

            var pos = splineSample.position;

            transform.position = pos;
        }

        yield return new WaitForSeconds(2);

        var level = PlayerPrefs.GetInt("level", 0);

        level++;

        PlayerPrefs.SetInt("level", level);

        var progress = PlayerPrefs.GetInt("progressionLevel", 1);

        progress++;

        PlayerPrefs.SetInt("progressionLevel", progress);

        OverlayCanvas.Instance.InitReloadScene();
    }
}
