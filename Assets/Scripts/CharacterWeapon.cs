using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.NiceVibrations;


public class CharacterWeapon : Colorable
{
    [SerializeField] private List<AudioClip> shootClips;

    [SerializeField] private List<Color> randomShootColors;

    [SerializeField] private AudioSource audioSource;

    [SerializeField] private Paintball paintball_prefab;

    [SerializeField] private ParticleSystem shootEffect;

    [SerializeField] private List<Renderer> magBalls;

    public Transform shootPoint, model;

    [SerializeField] private float smooth;

    private Vector3 shootDirection;

    public void Setup()
    { 
        foreach (var item in magBalls)
        {
            var color = randomShootColors[Random.Range(0, randomShootColors.Count)];

            item.material.color = color;
            item.material.SetColor(emissionColorProp, color * .6f);
        }
    }

    public void FireRequest(Vector3 targetPoint)
    {
        if (enabled) Shoot();

        Debug.DrawLine(transform.position, targetPoint, color, 4);

        var dir = targetPoint - model.position;

        shootDirection = dir;//model.InverseTransformDirection(targetPoint);

        enabled = true;
    }

    private void Update()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(shootDirection), smooth * Time.deltaTime);

        if(Vector3.Angle(transform.forward, shootDirection) <= 0.0f)
        {
            enabled = false;

            transform.forward = shootDirection;

            Shoot();
        }
    }

    protected virtual void Shoot()
    {
        StartCoroutine(Shooting());

        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    private IEnumerator Shooting()
    {
        model.DOComplete();

        model.DOPunchPosition(Vector3.back * .18f, .14f, 1);

        SetColor(randomShootColors[Random.Range(0, randomShootColors.Count)]);

        yield return new WaitForSeconds(.06f);

        var partsMat = shootEffect.GetComponent<ParticleSystemRenderer>().material;

        partsMat.SetColor(colorProp, color);
        partsMat.SetColor(emissionColorProp, color * .2f);

        audioSource.PlayOneShot(shootClips[Random.Range(0, shootClips.Count)]);

        shootEffect.Play();

        var ball = Instantiate(paintball_prefab);

        ball.transform.position = shootPoint.position;

        ball.SetColor(color);

        ball.Shoot(shootDirection);
    }
}
