using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsControl : Singleton<AnalyticsControl>
{
    void Start()
    {
        GameAnalyticsSDK.GameAnalytics.Initialize();
    }
}
