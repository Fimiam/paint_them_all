using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PaintCanvas : MonoBehaviour
{
    [SerializeField] private Transform paintPoint;

    [HideInInspector] public PaintableObject paintableObject;

    public new Camera camera;

    [SerializeField] private RenderTexture renderTexture_ref;

    [SerializeField] private RawImage paintImage;

    [HideInInspector] public RenderTexture paintTexture;

    [SerializeField] private List<PaintEffects> painting_prefabs;

    [SerializeField] private int effectsToSave = 5;

    private float defaultPaintPointOffset;

    private List<PaintEffect> effects = new List<PaintEffect>();

    private int effectsCount;

    public void Setup()
    {
        paintTexture = new RenderTexture(renderTexture_ref);

        camera.targetTexture = paintTexture;

        paintImage.texture = paintTexture;

        defaultPaintPointOffset = paintPoint.localPosition.z;
    }

    public void Paint(Vector2 textureCoord, Color color, PaintType paintType = PaintType.SPOT, bool damagable = false)
    {
        //Debug.Log("painting " + textureCoord);

        var position = new Vector3(Mathf.Lerp(-camera.orthographicSize, camera.orthographicSize, textureCoord.x),
            Mathf.Lerp(-camera.orthographicSize, camera.orthographicSize, textureCoord.y));

        position = paintPoint.position + position;

        if(effectsCount > effectsToSave)
        {
            //var tex = new Texture2D(paintTexture.width, paintTexture.height);

            //var temp = RenderTexture.active;

            //RenderTexture.active = paintTexture;

            //tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);

            //tex.Apply();

            //RenderTexture.active = temp;

            //paintImage.texture = tex;

            effects.ForEach(e => e.gameObject.SetActive(false));// Destroy(e.gameObject));

            effects.Clear();

            effectsCount = 0;

            paintPoint.localPosition = Vector3.forward * defaultPaintPointOffset;

            //paintImage.gameObject.SetActive(true);

            //var rt = new RenderTexture(paintTexture);

        }

        paintPoint.localPosition = Vector3.forward * ((1.0f - effectsCount / (float)effectsToSave) * defaultPaintPointOffset + 1.0f);

        var effectsToPaint = painting_prefabs.Find(e => e.type == paintType);

        var effect = Instantiate(effectsToPaint.effects[Random.Range(0, effectsToPaint.effects.Count)]);

        effect.transform.position = position;

        effect.transform.localScale = Vector3.one * paintableObject.paintScale;

        effect.transform.SetParent(transform);

        effectsCount++;

        effects.Add(effect);

        effect.SetColor(color);

        effect.Play();

        if (damagable)
        {
            Debug.Log("11e");

            paintableObject.OnGetPaint?.Invoke(color);
        }

    }


}

[Serializable]
public class PaintEffects
{
    public PaintType type;

    public List<PaintEffect> effects;
}

public enum PaintType
{
    SPOT = 0, LEAKING_SPOT = 1, PARTICLE_DROP = 2, BARREL_SPLASH = 3
}
