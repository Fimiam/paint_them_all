using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour
{
    public static EnemiesController Instance;

    private int currentEnemies;

    private bool activated;

    public bool IsCompleteEnemies;

    private void Awake()
    {
        Instance = this;
    }

    public void ActivateEnemies(int index)
    {
        if (activated) return;

        if (index > Level.Instance.enemyStages.Count - 1) return;

        foreach (var item in Level.Instance.enemyStages[index].enemies)
        {
            if(item.activableOnStart)
            {
                item.enabled = true;
            }
        }

        activated = true;

        currentEnemies = index;
    }

    public void EnemyDied(EnemyCharacter enemy)
    {
        if (Player.Instance.inTransition) return;

        foreach (var item in Level.Instance.enemyStages)
        {
            item.enemies.Remove(enemy);
        }

        if(Level.Instance.enemyStages[currentEnemies].enemies.Count < 1)
        {
            StageComplete();

            if(currentEnemies >= Level.Instance.enemyStages.Count - 1)
            {
                IsCompleteEnemies = true;
            }
        }
    }

    private void StageComplete()
    {
        activated = false;

        Player.Instance.StageComplete();
    }
}
