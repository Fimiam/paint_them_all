using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesGroup : MonoBehaviour
{
    public List<EnemyCharacter> GetEnemies()
    {
        var list = new List<EnemyCharacter>();

        foreach (var item in transform.GetComponentsInChildren<EnemyCharacter>())
        {
            list.Add(item);
        }

        return list;
    }
}
