using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintEffectSpot : PaintEffect
{
    [SerializeField] private SpriteRenderer spriteRenderer;

    [SerializeField] private List<Sprite> sprites;

    public override void Play()
    {
        
    }

    public override void SetColor(Color color)
    {
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Count)];

        spriteRenderer.transform.rotation = Quaternion.Euler(Vector3.forward * Random.value * 360);

        spriteRenderer.color = color;
    }
}
