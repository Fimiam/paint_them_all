using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private List<Level> levels;

    
    void Start()
    {
        var l = PlayerPrefs.GetInt("level", 0);

        if(l > levels.Count - 1)
        {
            l = 0;

            PlayerPrefs.SetInt("level", l);
        }

        var level = Instantiate(levels[l]);
    }
}
