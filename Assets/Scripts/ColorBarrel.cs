using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ColorBarrel : DestroyableObject
{
    [SerializeField] private new SkinnedMeshRenderer renderer;

    [SerializeField] private Vector3 explosionScale;

    [SerializeField] private List<Transform> bones;

    [SerializeField] private float explosionPaintRadius = 3;

    float maxHp;

    [SerializeField] private PaintSplashEffect splashEffect;

    
    void OnEnable()
    {
        //bones[1].DOScale(Vector3.one * 1.2f, .2f);
    }

    private void Start()
    {
        maxHp = health;
    }

#if UNITY_EDITOR

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.B))
        {
            GetDefaultDamage(Color.blue);
        }
    }

#endif

    public override void GetDefaultDamage(Color color)
    {
        base.GetDefaultDamage(color);

        renderer.material.color = color;

        renderer.material.SetColor("_EmissionColor", color * .4f);

        var lerp = 1.0f - (health / maxHp);

        bones[1].DOScale(Vector3.Lerp(Vector3.one, explosionScale, lerp), .2f);

        splashEffect.color = color;
    }

    public override void Death()
    {
        var ray = new Ray(transform.position + Vector3.up, Vector3.down);

        //RaycastHit[] hits = new RaycastHit[3];

        RaycastHit[] hits = Physics.SphereCastAll(ray, explosionPaintRadius);

        if(hits.Length > 0)
        {
            foreach (var item in hits)
            {
                FindObjectOfType<Control>().TryPaint(item, splashEffect.color, PaintType.BARREL_SPLASH);

                Debug.Log("95249854ugwr");
            }
        }

        //if (Physics.Raycast(ray, /*splashRadius,*/ out RaycastHit hit, Mathf.Infinity))
        //{
        //    //Debug.DrawRay(ray.origin, ray.direction * hit.distance, color, 20);
        //    //Debug.DrawRay(hit.point, hit.normal, color, 20);

        //    FindObjectOfType<Control>().TryPaint(hit, splashEffect.color, PaintType.BARREL_SPLASH);

        //    Debug.Log("95249854ugwr");
        //}

        base.Death();


        splashEffect.gameObject.SetActive(true);

        splashEffect.transform.SetParent(null);

        gameObject.SetActive(false);

    }
}
