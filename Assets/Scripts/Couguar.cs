using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Couguar : EnemyCharacter
{
    [SerializeField] private List<Transform> points;

    [SerializeField] private float distanceToSwitch = .2f;

    private int currentPoint;

    protected override void Start()
    {
        if(points.Count > 0)
            agent.SetDestination(points[currentPoint].position);
        else
        {
            var targetPos = Player.Instance.transform.position;

            agent.SetDestination(targetPos);
        }    
    }

    protected override void Update()
    {
        base.Update();

        if((transform.position - points[currentPoint].position).sqrMagnitude < distanceToSwitch)
        {
            currentPoint++;

            if(currentPoint > points.Count - 1)
            {
                agent.SetDestination(Player.Instance.transform.position);
            }
            else
            {
            
                agent.SetDestination(points[currentPoint].position);

            }

        }
    }

    public override void Death()
    {
        base.Death();

        animator.enabled = false;

        agent.enabled = false;
    }
}
