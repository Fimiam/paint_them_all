using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public static Level Instance;

    public List<StageEnemies> enemyStages;

    public List<SplineComputer> playerTracks;

    public List<Transform> playerPoints;

    private void Awake()
    {
        Instance = this;

        foreach (var item in transform.GetComponentsInChildren<EnemiesGroup>())
        {
            var stage = new StageEnemies();

            stage.enemies = item.GetEnemies();

            enemyStages.Add(stage);
        }
    }

    void Start()
    {
        
    }
}
