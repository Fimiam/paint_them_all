using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluidPainterHitEffect : Colorable
{
    [SerializeField] private new ParticleSystem particleSystem;
    [SerializeField] private LayerMask paintableMask;

    private Control control;

    private void Start()
    {
        control = FindObjectOfType<Control>();
    }

    public override void SetColor(Color color)
    {
        base.SetColor(color);

        var module = particleSystem.main;

        module.startColor = color;
    }

    void Update()
    {
        var ray = new Ray(transform.position - transform.forward, transform.forward);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, paintableMask))
        {
            control.TryPaint(hit, color, PaintType.SPOT, false);
        }
    }

    public void StopPlay()
    {
        particleSystem.Stop();
    }
}
