using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FluidPainter : Colorable
{
    [SerializeField] private new Renderer renderer;
    [SerializeField] private int colorMaterialIndex;
    [SerializeField] private Transform model, sqeezeBone, idlePoint;
    [SerializeField] private AnimationCurve squeezeCurve;
    [SerializeField] private float squeezeRevive = .5f;
    private float squeezeTime;

    [SerializeField] private Transform paintPoint;
    [SerializeField] private Vector3 paintReadyOffset, paintActiveOffset;
    [SerializeField] private float paintReadySmooth = .1f, paintActiveSmooth = .1f, startPaintDelay = .4f;
    [SerializeField] private LayerMask paintableMask;

    [SerializeField] private bool flipSides = true;

    [SerializeField] FinalPaintControl paintControl;

    [SerializeField] private ParticleSystem paintParticles;

    [SerializeField] private FluidPainterHitEffect hitEffectPrefab;

    private FluidPainterHitEffect activeHitEffect;

    private Vector3 targetPosition;

    private new Camera camera;

    private bool paintActive;

    private void Start()
    {
        camera = Camera.main;
    }

    public override void SetColor(Color color)
    {
        base.SetColor(color);

        renderer.materials[colorMaterialIndex].color = color;

        var module = paintParticles.main;

        module.startColor = color;
    }

    private void Update()
    {
        bool hold = Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject();


        var activation = Mathf.InverseLerp(.7f, 1.0f, paintControl.paintActiveValue);

        var paintOffset = Vector3.Lerp(paintReadyOffset, paintActiveOffset, activation);

        if(flipSides)
        {
            var camRelative = camera.transform.InverseTransformPoint(paintPoint.position);

            paintOffset.x = Mathf.Sign(camRelative.x) * paintReadyOffset.x;         
        }

        targetPosition = hold ? paintPoint.position + paintOffset : idlePoint.position;

        transform.position = Vector3.Lerp(transform.position, targetPosition, paintActiveSmooth);

        var paintDir = paintPoint.position - transform.position;

        transform.rotation = Quaternion.Lerp(idlePoint.rotation, Quaternion.LookRotation(paintDir), activation);

        bool active = activation > .99f;

        if(active)
        {
            squeezeTime += Time.deltaTime;

            sqeezeBone.localScale = Vector3.one * squeezeCurve.Evaluate(squeezeTime);

            if(!paintParticles.isPlaying)
            {
                paintParticles.Play();
            }
        }
        else
        {
            squeezeTime = 0.0f;
            sqeezeBone.localScale = Vector3.Lerp(sqeezeBone.localScale, Vector3.one, squeezeRevive);
        }

        paintActive = squeezeTime > startPaintDelay;

        if(paintActive)
        {
            //var control = FindObjectOfType<Control>();

            var ray = camera.ScreenPointToRay(camera.WorldToScreenPoint(paintPoint.position));

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, paintableMask))
            {
                //control.TryPaint(hit, control.debugColor, PaintType.SPOT, false);

                if(activeHitEffect == null)
                {
                    activeHitEffect = Instantiate(hitEffectPrefab);

                    activeHitEffect.SetColor(color);
                }

                activeHitEffect.transform.position = hit.point;
                activeHitEffect.transform.forward = -hit.normal;
            }
            else
            {
                if(activeHitEffect)
                {
                    activeHitEffect.StopPlay();
                    Destroy(activeHitEffect.gameObject, 3);
                    activeHitEffect = null;
                }
            }
        }
        else
        {
            if(paintParticles.isPlaying)
            {
                paintParticles.Stop();

                var newEffect = Instantiate(paintParticles);

                newEffect.transform.SetParent(paintParticles.transform.parent);
                newEffect.transform.SetPositionAndRotation(paintParticles.transform.position, paintParticles.transform.rotation);
                newEffect.transform.localScale = paintParticles.transform.localScale;

                paintParticles.transform.SetParent(null);
                paintParticles.transform.forward = paintPoint.position - paintParticles.transform.position;

                Destroy(paintParticles.gameObject, 3f);

                paintParticles = newEffect;
            }


            if (activeHitEffect)
            {
                activeHitEffect.StopPlay();
                Destroy(activeHitEffect.gameObject, 3);
                activeHitEffect = null;
            }
        }
    }
}
