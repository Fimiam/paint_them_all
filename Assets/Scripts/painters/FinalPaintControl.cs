using UnityEngine;
using UnityEngine.UI;

public class FinalPaintControl : MonoBehaviour
{
    [SerializeField] private Transform paintPoint;
    [SerializeField] private Transform aimTransform;
    [SerializeField] private Image aim_Image;

    [SerializeField] private float aimSizeModifier = 5;

    [SerializeField] private LayerMask aimTriggerMask, paintableMask;

    [SerializeField] private Vector3 aimOffset;
    [SerializeField] private float aimPositionSmooth = .5f;
    [SerializeField] private float paintActivationSpeed = 1f;
    [SerializeField] private float paintReleaseSpeed = 1;
    [SerializeField] private float distanceToPaintFill = 2;

    private Vector3 targetPosition, lastPosition;

    private float lastAimDist;
    public float paintActiveValue;

    private PaintableObject paintable;

    public bool hasPaintable => paintable != null;

    private void Start()
    {
        var tr = FindObjectOfType<PaintAimTrigger>();

        if (tr) lastAimDist = tr.aimDistance;
    }

    private void Update()
    {
        var camera = Camera.main;

        bool hold = Input.GetMouseButton(0);

        if (hold)
        {
            var ray = camera.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, aimTriggerMask))
            {
                var targetPoint = hit.point + aimOffset;

                targetPosition = targetPoint;

                if(hit.collider.TryGetComponent<PaintAimTrigger>(out PaintAimTrigger trigger))
                {
                    lastAimDist = trigger.aimDistance;
                }
            }
        }

        paintPoint.position = Vector3.Lerp(paintPoint.position, targetPosition, aimPositionSmooth);

        var camPos = camera.transform.position;

        var camDir = (camPos - paintPoint.position).normalized;

        aimTransform.position = paintPoint.position + camDir * lastAimDist;

        var aimCamDist = (aimTransform.position - camPos).magnitude;

        aimTransform.localScale = Vector3.one * (aimCamDist / aimSizeModifier);

        if(hold)
        {
            var magn = (paintPoint.position - targetPosition).magnitude;

            //Debug.Log(magn);

            var ray = camera.ScreenPointToRay(camera.WorldToScreenPoint(paintPoint.position));

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, paintableMask))
            {
                paintable = hit.collider.GetComponent<PaintableObject>();
            }
            else
                paintable = null;

            if (magn < distanceToPaintFill && hasPaintable)
            {
                paintActiveValue = Mathf.MoveTowards(paintActiveValue, 1.0f, paintActivationSpeed * Time.deltaTime);
            }
            else 
            {
                if (paintActiveValue < 1.0f || !hasPaintable)
                    paintActiveValue = Mathf.Lerp(paintActiveValue, 0f, paintReleaseSpeed);
            }
        }
        else
        {
            paintActiveValue = Mathf.Lerp(paintActiveValue, 0f, paintReleaseSpeed);
        }

        aim_Image.fillAmount = paintActiveValue;
    }
}
