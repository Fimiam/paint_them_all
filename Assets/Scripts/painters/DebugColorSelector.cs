using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugColorSelector : MonoBehaviour
{
    [SerializeField] private Colorable colorable;

    private void Start()
    {
        SelectColor(0);
    }

    public void SelectColor(int index)
    {
        colorable.SetColor(transform.GetChild(index).GetComponent<Image>().color);
    }
}
