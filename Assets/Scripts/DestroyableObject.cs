using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour
{
    [SerializeField] protected float health;

    [SerializeField] protected float defaultTakenDamage;

    public bool isDead;

    [SerializeField] private PaintableObject relatedPaintable;

    protected virtual void Awake()
    {
        if (relatedPaintable)
        {
            relatedPaintable.OnGetPaint += GetDefaultDamage;
        }
    }

    public virtual void GetDefaultDamage(Color color)
    {
        if (isDead) return;

        health -= defaultTakenDamage;

        if(health <= 0)
        {
            Death();
        }
    }

    public virtual void Death()
    {
        isDead = true;
    }
}
