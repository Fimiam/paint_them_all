using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableWeapon : DestroyableObject
{
    [SerializeField] private Collider colliderToEnable, colliderToDisable;

    [SerializeField] private Rigidbody rb;

    public override void Death()
    {
        base.Death();

        colliderToEnable.enabled = true;
        colliderToDisable.enabled = false;

        rb.isKinematic = false;

        rb.useGravity = true;

        transform.SetParent(null);
    }
}
