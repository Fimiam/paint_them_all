using Deform;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushDeformable : MonoBehaviour
{
    [SerializeField] private BendDeformer bendDeformer;

    [SerializeField] private Animator animator;

    [SerializeField] private ParticleSystem particleSystem;

    [SerializeField] private float addAngle = -30;

    [SerializeField] private Transform inputPlanePoint, brushTransform;

    [SerializeField] private PaintableObject paintPart;

    [SerializeField] private Colorable parts;

    private Vector3 tapPoint, releasePoint;

    private bool waitingForRelease, wetWaiting;

    private Plane inputPlane;

    private Color colorToWet;

    private void Start()
    {
        inputPlane = new Plane(inputPlanePoint.forward, inputPlanePoint.position);
    }

    public void Paint()
    {
        particleSystem.transform.rotation = Quaternion.LookRotation(brushTransform.up);

        var rot = particleSystem.transform.localEulerAngles;

        rot.x += addAngle;

        particleSystem.transform.localRotation = Quaternion.Euler(rot);

        particleSystem.Play();
    }

    public void Wet()
    {
        Debug.Log("wet");

        Control.Instance.TryPaint(paintPart, new Vector2(Random.Range(.45f, .65f), .8f), colorToWet);

        parts.SetColor(colorToWet);
    }

    private void Update()
    {
        if (wetWaiting) return;

        if (Input.GetMouseButtonDown(0))
        {
            waitingForRelease = true;

            tapPoint = GetPointOnPlane();
        }

        if (Input.GetMouseButtonUp(0))
        {
            releasePoint = GetPointOnPlane();

            var dir = releasePoint - transform.position;

            dir.y = 0;

            transform.rotation = Quaternion.LookRotation(dir);

            waitingForRelease = false;
            animator.SetTrigger("paint");


            Debug.DrawRay(tapPoint, Vector3.back, Color.black, 5);
            Debug.DrawRay(releasePoint, Vector3.back, Color.red, 5);
        }
    }

    private Vector3 GetPointOnPlane()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float enter;

        inputPlane.Raycast(ray, out enter);

        return ray.GetPoint(enter);
    }

    public void IntoTheBucket(PaintBucket bucket, Color color)
    {
        colorToWet = color;
        StartCoroutine(IntoBucketMove(bucket));
    }

    private IEnumerator IntoBucketMove(PaintBucket bucket)
    {
        wetWaiting = true;

        transform.DOMoveX(bucket.transform.position.x, .3f);

        transform.DORotateQuaternion(bucket.transform.rotation, .2f);

        yield return new WaitForSeconds(.2f);

        animator.SetTrigger("wet");

        yield return new WaitForSeconds(.5f);

        //Debug.Log(animator.GetCurrentAnimatorStateInfo(0).ToString());

        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("brushIdle"));

        Debug.Log("wet done");

        wetWaiting = false;
    }
}
