using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UI : MonoBehaviour
{
    [SerializeField] private Text levelText, shootText;

    private InputController inputController;

    void Start()
    {
        levelText.text = $"LEVEL {PlayerPrefs.GetInt("progressionLevel", 1)}";

        //var seq = DOTween.Sequence();

        //seq.AppendInterval(2f);
        //seq.Append(levelText.DOFade(0, 1f));

        //seq.onComplete += () => { levelText.gameObject.SetActive(false); };

        inputController = FindObjectOfType<InputController>();

        StartCoroutine(HintProcess());
    }

    private IEnumerator HintProcess()
    {
        shootText.rectTransform.DOPunchScale(Vector2.one * .2f, 2, 3).SetLoops(-1);

        yield return new WaitUntil(() => Input.GetMouseButtonDown(0) && !inputController.IsPointerOverUI);

        levelText.DOFade(0, .6f).onComplete += () => { levelText.gameObject.SetActive(false); };

        shootText.DOFade(0, .6f).onComplete += () => { shootText.gameObject.SetActive(false); };
    }
}
