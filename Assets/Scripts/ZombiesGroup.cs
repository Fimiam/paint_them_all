using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiesGroup : MonoBehaviour
{
    private bool activated;

    public void ZombieHit()
    {
        if (activated) return;

        activated = true;

        Debug.Log("zombieGroup activated");

        foreach (var item in transform.GetComponentsInChildren<EnemyZombie>())
        {
            item.enabled = true;
        }
    }
}
