using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyPanel : MonoBehaviour
{
    public static MoneyPanel Instance;

    [SerializeField] private MoneyAddEffect addEffect_prefab;

    [SerializeField] private RectTransform moneyIconRect;

    [SerializeField] private Text money_Text;

    [SerializeField] private Canvas canvas;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        money_Text.text = $"{PlayerPrefs.GetInt("money", 0)}";
    }

    public void AddMoney(Vector3 worldPos, int money)
    {
        var viewPort = Camera.main.WorldToViewportPoint(worldPos);

        var anchPos = new Vector2(canvas.GetComponent<RectTransform>().rect.width * viewPort.x, canvas.GetComponent<RectTransform>().rect.height * viewPort.y);

        var effect = Instantiate(addEffect_prefab);

        effect.transform.SetParent(transform);

        effect.transform.localScale = Vector3.one;

        effect.AddMoney(anchPos, moneyIconRect.position, money);
    }

    public void AddRealMoney(int value)
    {
        var m = PlayerPrefs.GetInt("money", 0);

        m += value;

        money_Text.text = $"{m}";

        PlayerPrefs.SetInt("money", m);
    }
}
