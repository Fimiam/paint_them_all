using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinnedMeshCollider : MonoBehaviour
{
    [SerializeField] private float updatePerSeconds = 4;

    [SerializeField] private SkinnedMeshRenderer skinnedMesh;

    [SerializeField] private MeshCollider meshCollider;

    private void Start()
    {
        StartCoroutine(UpdatingProcess());
    }

    private IEnumerator UpdatingProcess()
    {
        Mesh mesh = new Mesh();


        while(true)
        {
            skinnedMesh.BakeMesh(mesh);

            meshCollider.sharedMesh = mesh;

            yield return new WaitForSeconds(1.0f / updatePerSeconds);
        }
    }
}
