using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Picture : MonoBehaviour
{
    [SerializeField] private PaintableObject paintable;

    [SerializeField] private int paintsToComplete = 5;

    [SerializeField] private Image timer_Image;

    [SerializeField] private float paintTimer = 8;

    [SerializeField] private MeshCollider meshCollider;

    [SerializeField] private MagnGlass magnGlass;

    [SerializeField] private Text hit_Text;

    private bool paintStarted;

    // Start is called before the first frame update
    void Start()
    {
        paintable.OnGetPaint += OnPaint;
    }

    private void OnPaint(Color color)
    {
        paintStarted = true;

        //paintsToComplete--;

        //if (paintsToComplete <= 0) CompletePicture();
    }

    public void StartPainting()
    {
        gameObject.SetActive(true);

        meshCollider.enabled = true;

        StartCoroutine(PaintProcess());
    }

    private IEnumerator PaintProcess()
    {
        hit_Text.rectTransform.DOPunchScale(Vector2.one * .1f, 4, 2);

        yield return new WaitUntil(() => paintStarted);

        hit_Text.DOFade(0, .2f).onComplete += () => hit_Text.gameObject.SetActive(false);

        float t = 0.0f;

        while(t < paintTimer)
        {
            t += Time.deltaTime;

            timer_Image.fillAmount = 1.0f - t / paintTimer;

            yield return null;
        }

        CompletePicture();
    }

    private void CompletePicture()
    {
        magnGlass.gameObject.SetActive(true);


    }
}
