﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RateUsPopup : MonoBehaviour
{
    private const int STARS_TO_RATE = 4;
    private const string EMAILL_TO_SEND = "xsandxbox@gmail.com";
    private const string SUBJECT_TO_SEND = "Feedback about Paint them all";

    [SerializeField]
    private List<Button> stars;

    [SerializeField]
    private Button sumbit_Button, later_Button;

    [SerializeField] private GameObject view;

    private void Start()
    {
        stars.ForEach(s => s.onClick.AddListener(() => StarClicked(s.transform.GetSiblingIndex())));

        sumbit_Button.onClick.AddListener(SumbitClicked);
        later_Button.onClick.AddListener(LaterClicked);
    }

    public void Show()
    {
        view.SetActive(true);

        CleanStars();
    }

    private void StarClicked(int _index)
    {
        CleanStars();

        for (int i = 0; i < _index + 1; i++)
        {
            stars[i].transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    private void SumbitClicked()
    {
        int stars = this.stars.FindAll(s => s.transform.GetChild(0).gameObject.activeSelf).Count;

        if(stars < STARS_TO_RATE)
        {
            SendEmail();
        }
        else
        {
            Application.OpenURL("market://details?id=" + Application.productName);
        }

    }

    private void CleanStars()
    {
        stars.ForEach(s => s.transform.GetChild(0).gameObject.SetActive(false));
    }

    private void LaterClicked()
    {
        gameObject.SetActive(false);
    }

    private void SendEmail()
    {
        string email = EMAILL_TO_SEND;
        string subject = MyEscapeURL(SUBJECT_TO_SEND);
        string body = MyEscapeURL("poor stars");

        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    private string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}
