using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    [Header("Android")]
    //[SerializeField] private string appID_Android = "";
    [SerializeField] private string bannerID_Android = "";
    [SerializeField] private string interstitialID_Android = "";
    [SerializeField] private string rewardedID_Android = "";

    [Header("IOS")]
    //[SerializeField] private string appID_IOS = "";
    [SerializeField] private string bannerID_IOS = "";
    [SerializeField] private string interstitialID_IOS = "";
    [SerializeField] private string rewardedID_IOS = "";

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardedAd rewardedAd;

    private bool bannerShown, bannerLoaded;

    public bool IsRewardedReady => rewardedAd.IsLoaded();

    public bool adsActive;

    private void Awake()
    {
        Instance = this;

        adsActive = PlayerPrefs.GetInt("ads_active", 1) > 0;
    }

    void Start()
    {
        MobileAds.Initialize( status => 
        {
            RequestBanner();
            RequestInterstitial();
            RequestRewarded();
        });
    }

    private void RequestRewarded()
    {
#if UNITY_ANDROID
        string adUnitId = interstitialID_Android;
#elif UNITY_IPHONE
        string adUnitId = interstitialID_IOS;
#else
        string adUnitId = interstitialID_IOS;
#endif

        rewardedAd = new RewardedAd(adUnitId);
        
        AdRequest request = new AdRequest.Builder().Build();
        
        rewardedAd.LoadAd(request);

        rewardedAd.OnUserEarnedReward += RewardedComplete;
    }

    private void RewardedComplete(object sender, Reward e)
    {
        Debug.Log("continue rewarded");

        //Gameplay.Instance.ContinueReward();
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = interstitialID_Android;
#elif UNITY_IPHONE
        string adUnitId = interstitialID_IOS;
#else
        string adUnitId = interstitialID_IOS;
#endif

        interstitial = new InterstitialAd(adUnitId);

        AdRequest request = new AdRequest.Builder().Build();
        
        interstitial.LoadAd(request);

        interstitial.OnAdClosed += InterstitialClosed;
    }

    private void InterstitialClosed(object sender, EventArgs e)
    {
        Debug.Log("interstitial closed, requesting a new one");

        interstitial.OnAdClosed -= InterstitialClosed;
        interstitial.Destroy();

        RequestInterstitial();
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
            string adUnitId = bannerID_Android;
#elif UNITY_IPHONE
            string adUnitId = bannerID_IOS;
#else
            string adUnitId = bannerID_IOS;
#endif

        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        bannerView.OnAdLoaded += BannerLoaded;
        bannerView.OnAdFailedToLoad += BannerFailToLoad;
    }

    private void BannerFailToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        
    }

    private void BannerLoaded(object sender, EventArgs e)
    {
        bannerLoaded = bannerShown = true;
    }

    public void ShowBanner()
    {
        Debug.Log("try to show banner");

        if (!adsActive || bannerShown) return;

        if(bannerLoaded)
        {
            bannerView.Show();
        }
        else
        {
            AdRequest request = new AdRequest.Builder().Build();   
            bannerView.LoadAd(request);
        }
    }

    public void HideBanner()
    {
        bannerShown = false;

        bannerView.Hide();
    }

    public void ShowInterstitial()
    {
        Debug.Log("try to show interstitial");

        if(adsActive && interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    public void ShowRewarded()
    {
        Debug.Log("try to show rewarded");

        rewardedAd.Show();
    }

    public void DisableAds()
    {
        Debug.Log("disabling ads");

        PlayerPrefs.SetInt("ads_active", 0);

        //Game.Instance.ProgressData.BoughtNoAds();

        adsActive = false;

        HideBanner();

        bannerView.Destroy();

        interstitial.Destroy();

        //var noAdsButton = FindObjectOfType<NoAdsButton>();

        //if (noAdsButton) noAdsButton.gameObject.SetActive(false);
    }
}
