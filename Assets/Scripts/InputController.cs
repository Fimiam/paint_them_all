﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour
{
    [SerializeField] private Camera inputCamera;

    private Plane inputPlane;

    public static Vector2 PlayerInput { get; set; }

    public static bool Hold => Input.GetMouseButton(0);

    private Vector3 lastInput;

    public bool IsPointerOverUI
    {
        get
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return EventSystem.current.IsPointerOverGameObject();
#else
            return IsPointerOverUIObject();
#endif
        }
    }

    private void Start()
    {
        inputPlane = new Plane(Vector3.back, Vector3.forward * 10);
    }

    void Update()
    {
        PlayerInput = Vector2.zero;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = inputCamera.ScreenPointToRay(Input.mousePosition);

            float enter;

            inputPlane.Raycast(ray, out enter);

            Vector3 point = ray.GetPoint(enter);

            lastInput = point;
        }

        if (Input.GetMouseButton(0))
        {
            Ray ray = inputCamera.ScreenPointToRay(Input.mousePosition);

            float enter;

            inputPlane.Raycast(ray, out enter);

            Vector3 point = ray.GetPoint(enter);

            PlayerInput = point - lastInput;

            lastInput = point;
        }
    }

    bool IsPointerOverUIObject()
    {
        if (Input.touchCount < 1) return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
