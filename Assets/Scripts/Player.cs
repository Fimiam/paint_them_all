using DG.Tweening;
using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance;

    [SerializeField] private int health = 10; 

    [SerializeField] private CharacterWeapon weapon;

    [SerializeField] private Transform cameraTransform;

    [SerializeField] private AnimationCurve walkWaving;

    [SerializeField] private float targetDistance;

    [SerializeField] private LayerMask targetMask;

    [SerializeField] private float trackMovementSpeed = 7;

    [SerializeField] private float fireRate = .1f;

    private float nextFire;

    public int currentStage;
    public bool inTransition;

    private InputController inputController;

    bool started;

    private void Awake()
    {
        Instance = this;

    }

    private void Start()
    {
        transform.position = Level.Instance.playerPoints[currentStage].position;
        transform.rotation = Level.Instance.playerPoints[currentStage].rotation;

        inputController = FindObjectOfType<InputController>();

        weapon.Setup();
    }

    void Update()
    {
        var eulers = transform.eulerAngles;

        eulers.y += InputController.PlayerInput.x * 5;
        eulers.x -= InputController.PlayerInput.y * 5;

        transform.rotation = Quaternion.Euler(eulers);


        if(!inTransition && Input.GetMouseButton(0) && !inputController.IsPointerOverUI)
        {

            if(!started)
            {
                started = true;

                if (!EnemiesController.Instance.IsCompleteEnemies)
                    EnemiesController.Instance.ActivateEnemies(currentStage);
            }

            if(Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;

                Shoot();
            }

        }
        else
        {
            nextFire = 0;
        }
    }

    private void Shoot()
    {
        var ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width / 2.0f, Screen.height / 2.0f));

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, targetMask))
        {
            Debug.Log(hit.collider.name);

            weapon.FireRequest(hit.point);
        }
        else
        {
            weapon.FireRequest(ray.origin + (ray.direction * 10.0f));
        }
    }

    public void GetDamage()
    {
        if (health < 1) return;

        health--;

        OverlayCanvas.Instance.ShowHit();

        if(health < 1)
        {
            OverlayCanvas.Instance.InitReloadScene(true);
        }
    }

    public void StageComplete()
    {
        currentStage++;

        StartCoroutine(StageTransition());
    }

    private IEnumerator StageTransition()
    {
        inTransition = true;

        yield return new WaitForSeconds(1.0f);

        var moveSpline = Level.Instance.playerTracks[currentStage - 1];

        var trackLength = moveSpline.CalculateLength();

        var currDistance = 0.0f;

        float moveTime = 0.0f;

        SplineSample splineSample = default;

        while(currDistance < trackLength * .9f)
        {
            splineSample = moveSpline.Evaluate(currDistance / trackLength);

            transform.position = splineSample.position;

            var dir = splineSample.forward;

            dir.y = 0.0f;

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), .2f);

            cameraTransform.localPosition = Vector3.up * walkWaving.Evaluate(moveTime);

            currDistance += Time.deltaTime * trackMovementSpeed;
            
            moveTime += Time.deltaTime;

            yield return null;
        }

        var seq = DOTween.Sequence();

        seq.Append(transform.DOMove(Level.Instance.playerPoints[currentStage].position, .6f));
        seq.Join(transform.DORotateQuaternion(Level.Instance.playerPoints[currentStage].rotation, .6f));

        yield return new WaitForSeconds(.7f);

        inTransition = false;

        if(currentStage >= Level.Instance.playerPoints.Count - 1)
        {
            var level = PlayerPrefs.GetInt("level", 1);

            if(level > 0)
            {
               // AdsManager.Instance.ShowInterstitial();
            }

            FindObjectOfType<Picture>(true).StartPainting();
        }
        else
        {
            EnemiesController.Instance.ActivateEnemies(currentStage);
        }

    }
}

[System.Serializable]
public class StageEnemies
{
    public List<EnemyCharacter> enemies;
}
