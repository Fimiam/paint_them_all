using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyCharacterKnight : EnemyCharacter
{
    [SerializeField] private DestroyableWeapon shield, sword;

    [SerializeField] private Vector3 blockShieldRotation;

    protected override void Start()
    {
        base.Start();

        //StartCoroutine(BlockDelay());

    }

    private IEnumerator BlockDelay()
    {
        yield return new WaitForSeconds(6);

        animator.SetTrigger("block");

        shield.transform.DOLocalRotate(blockShieldRotation, .15f);

        agent.isStopped = true;
    }

    public override void Death()
    {
        base.Death();

        animator.enabled = false;

        agent.enabled = false;

        shield.Death();
        sword.Death();
    }
}
